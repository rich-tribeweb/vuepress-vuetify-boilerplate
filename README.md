# Vuepress 1.x & Vuetify 2.2.x Boilerplate

This is a successful attempt (finally!) to get Vuetify and Vuepress to play nicely together.
It was relatively easy with Vuetify 1.5 but since moving to 2.x, the previous method does not to work.

Here's a [live deploy](https://quirky-beaver-b01d70.netlify.com) on Netlify.

## Installation

1. Clone/download to new folder
2. `yarn install`
3. `yarn docs:dev` to serve (hot reload etc)
4. `yarn docs:build` to populate `dist` folder

## Features

### PWA

- Example manifest file
- Styled update popup using toast UI component
- PWA compat - the easiest way to add all the icons & splashscreens etc

### Styling

- Define theme-wide colours in `theme/plugins/vuetify.js`. This [theme generator](https://lobotuerto.com/vuetify-color-theme-builder/) is very useful
- Uses the new (since Vuetify 2.2) `presets` option - that mimics the Material Design Studies
- Use Vuetify material colours in single file templates
- Switch between dark & light modes
- Uses Fontawesome icons

### Layouts

- GlobalLayout.vue has been used in order to facilitate an app-bar, navigation-drawer & footer etc

## Notes

### enhanceApp.js & /plugins/vuetify.js

Inspired by [this article](https://www.telerik.com/blogs/up-and-running-with-vuepress), this configuration seems to work like a dream. It means the Vuetify [theme configuration](https://vuetifyjs.com/en/customization/theme) & [presets](https://dev.vuetifyjs.com/en/customization/presets) work.

### GlobalLayout.vue

Remove the duplicate `id="app"` on the outer `v-application` wrapper by defining a different id in the template

The directive does 2 things:

1. Moves the `data-app` atribute to the vuepress `id="app"` div
2. Moves the `global-ui` div from outside the outer v-application wrapper and places it within the `v-application--wrap` div. This ensures that any components in the theme's global-components (like the update popup) can use Vuetify components.

The app-bar, navigation-drawer & footer are wrapped in `<ClientOnly>` so that they are not included in the pre-rendering. My experience is that if this is not done, as soon as you get past 20 pages or so, the [build fails](https://github.com/vuejs/vuepress/issues/1878).

### theme/index.js

The `package.json` and `index.js` files ensure that there's no theme error reported when building.
It may be possible to put theme specific config in `index.js` instead of `.vuepress/config.js`

## Optimisations

- Vuetify is tree-shaken when building
- Lodash is loaded a function at at time to minimise size
- Moment is loaded without language files (thanks to [this article](https://medium.com/js-dojo/how-to-reduce-your-vue-js-bundle-size-with-webpack-3145bf5019b7))
