import vuetify from './plugins/vuetify'
const _ = {}
import merge from 'lodash.merge'
_.merge = merge

import moment from 'moment/src/moment'

import '@fortawesome/fontawesome-pro/css/all.css'

export default ({
  Vue, // the version of Vue being used in the VuePress app
  options, // the options for the root Vue instance
  router, // the router instance for the app
  siteData // site metadata
}) => {
  options.vuetify = vuetify
  Object.defineProperty(Vue.prototype, '$_', { value: _ })
  Object.defineProperty(Vue.prototype, '$moment', { value: moment })
}
