import Vue from "vue";
import Vuetify, {
  VApp,
  VBtn,
  VNavigationDrawer,
  VAppBar,
  VContent,
  VContainer,
  VFooter,
  VSnackbar,
  VIcon,
  VList,
  VListItem,
  VListItemTitle,
  VListItemAction,
  VListItemContent,
  VToolbarTitle,
  VAppBarNavIcon,
  VCard,
  VCardActions,
  VListItemSubtitle,
  VListItemAvatar
} from "vuetify/lib";
import { preset } from "vue-cli-plugin-vuetify-preset-crane/preset";

Vue.use(Vuetify, {
  components: {
    VApp,
    VBtn,
    VNavigationDrawer,
    VAppBar,
    VContent,
    VContainer,
    VFooter,
    VSnackbar,
    VIcon,
    VList,
    VListItem,
    VListItemTitle,
    VListItemAction,
    VListItemContent,
    VToolbarTitle,
    VAppBarNavIcon,
    VCard,
    VCardActions,
    VListItemSubtitle,
    VListItemAvatar
  }
});

//import LRU from "lru-cache";
// const themeCache = new LRU({
//   max: 10,
//   maxAge: 1000 * 60 * 60 // 1 hour
// });

const opts = {
  icons: {
    iconfont: "fal"
  },
  rtl: false,
  theme: {
    options: {
      minifyTheme: function(css) {
        return process.env.NODE_ENV === "production"
          ? css.replace(/[\r\n|\r|\n]/g, "")
          : css;
      },
      //themeCache,
      customProperties: true
    },
    dark: false,
    themes: {
      dark: {
        //primary: "#21CFF3",
        // accent: "#FF4081",
        // secondary: "#ffe18d",
        // success: "#4CAF50",
        // info: "#2196F3",
        // warning: "#FB8C00",
        //error: "#FF5252"
      },
      light: {
        //primary: "#4caf50",
        // accent: "#e91e63",
        // secondary: "#30b1dc",
        // success: "#4CAF50",
        // info: "#2196F3",
        // warning: "#FB8C00",
        //error: "#FF5252"
      }
    }
  }
};

export default new Vuetify({ preset, ...opts });
