const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin

module.exports = {
  configureWebpack: {
    plugins: [new BundleAnalyzerPlugin()],
    resolve: {
      alias: {
        moment: 'moment/src/moment'
      }
    }
  },
  title: 'Vuepress & Vuetify Boilerplate',
  base: '/',
  description: 'Yay - it works!',
  head: [
    ['meta', { name: 'theme-color', content: '#ffffff' }],
    ['link', { href: '/manifest.json', rel: 'manifest' }],
    [
      'script',
      {
        async: true,
        src: 'https://cdn.jsdelivr.net/npm/pwacompat@2.0.10/pwacompat.min.js',
        integrity:
          'sha384-I1iiXcTSM6j2xczpDckV+qhhbqiip6FyD6R5CpuqNaWXvyDUvXN5ZhIiyLQ7uuTh',
        crossorigin: 'anonymous'
      }
    ]
  ],
  dest: 'dist',
  plugins: [
    ['@vuepress/back-to-top', true],
    [
      '@vuepress/pwa',
      {
        serviceWorker: true,
        popupComponent: 'MySWUpdatePopup',
        updatePopup: {
          message: 'New content available',
          buttonText: 'Update'
        }
      }
    ],
    [
      'container',
      {
        type: 'tip',
        before: info =>
          `<div class="tip custom-block"><i class="fal fa-sparkles" color="blue"></i><p class="custom-block-title">${info}</p>`,
        after: '</div>'
      }
    ],
    [
      'container',
      {
        type: 'bluebox',
        before: info =>
          `<div class="bluebox custom-block"><i class="fal fa-sparkles bluebox-icon"></i><span class="custom-block-title">&nbsp;&nbsp;${info}: </span>`,
        after: '</div>'
      }
    ]
  ],
  themeConfig: {}
}
